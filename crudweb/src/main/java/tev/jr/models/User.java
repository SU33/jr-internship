package tev.jr.models;

import java.util.Date;

/**
 * Created by evg-us-jrcrud on 30.06.17.
 */
public class User {
    private int idSpecial;
    private String nameSpecial;
    private int ageSpecial;
    private boolean isAdminSpecial;
    private Date dateSpecial;

    public User() {
    }

    public User(int idSpecial, String nameSpecial, int ageSpecial, boolean isAdminSpecial, Date dateSpecial) {

        this.idSpecial = idSpecial;
        this.nameSpecial = nameSpecial;
        this.ageSpecial = ageSpecial;
        this.isAdminSpecial = isAdminSpecial;
        this.dateSpecial = dateSpecial;
    }

    public void setIsAdminSpecial(boolean isAdminSpecial) {
        this.isAdminSpecial = isAdminSpecial;
    }

    public int getIdSpecial() {
        return idSpecial;
    }

    public boolean getIsAdminSpecial() {
        return isAdminSpecial;
    }

    public void setIdSpecial(int idSpecial) {
        this.idSpecial = idSpecial;
    }

    public String getNameSpecial() {
        return nameSpecial;
    }

    public void setNameSpecial(String nameSpecial) {
        this.nameSpecial = nameSpecial;
    }

    public int getAgeSpecial() {
        return ageSpecial;
    }

    public Date getDateSpecial() {
        return dateSpecial;
    }

    public void setDateSpecial(Date dateSpecial) {
        this.dateSpecial = dateSpecial;
    }

    public void setAgeSpecial(int ageSpecial) {
        this.ageSpecial = ageSpecial;
    }
}
