package tev.jr.models;

/**
 * Created by evg-us-jrcrud on 02.07.17.
 */
public abstract class Base {
    protected int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}