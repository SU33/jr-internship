package tev.jr.tools;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import tev.jr.models.Role;
import tev.jr.models.User;
import tev.jr.store.Storages;

/**
 * Created by evg-us-jrcrud on 02.07.17.
 */
public class DbTool {
    public static void main(String[] args) {
//        readDB_variantA();
//        readDB_variantB();

    }

    public static void readDB_variantA() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");
        Storages storages = context.getBean(Storages.class);

        for (User element : storages.userStorage.values()) {
            String result = String.format("age=%d, name=%s", element.getAgeSpecial(), element.getNameSpecial());
            System.out.println(result);
        }
    }

    public static void readDB_variantB() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");
        Storages storages = context.getBean(Storages.class);
        for (Role element : storages.roleStorage.values()) {
            System.out.println(element.getName() + ":" + element.getId());
        }
    }
}
