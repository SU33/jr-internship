package tev.jr.tools;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import tev.jr.models.Role;
import tev.jr.models.User;
import tev.jr.store.RoleStorage;
import tev.jr.store.Storages;
import tev.jr.store.UserStorage;

import java.util.Date;

/**
 * Created by evg-us-jrcrud on 02.07.17.
 */
public class DbFill {
    public static void main(String[] args) {
//        fillUsersA();
        fillRoles();
    }

    public static void fillUsersA() {
        for (int i = 101; i < 200; i++) {
            UserStorage userStorage = new UserStorage();
            userStorage.add(new User(i, "testFillDb" + i, 10 * i, false, new Date()));
        }
    }


    public static void fillRoles() {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");
        Storages storages = context.getBean(Storages.class);
        for (int i = 120; i < 1100; i++) {
            storages.roleStorage.add(new Role(i, "testFillRoll" + i));
        }
    }
}
