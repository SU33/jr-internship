package tev.jr.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tev.jr.models.Role;
import tev.jr.store.Storages;

/**
 * Created by evg-us-jrcrud on 02.07.17.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private Storages storages;

    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    public String showRoles(ModelMap model) {
        model.addAttribute("roles", storages.roleStorage.values());
        return "admin/roles"; //.jsp автоматически подставится
    }

    @RequestMapping(value="/new",method = RequestMethod.POST)
    public String saveRole(@ModelAttribute Role role, ModelMap modelMap) {
        storages.roleStorage.add(role);
        return "redirect:roles";
    }
}
