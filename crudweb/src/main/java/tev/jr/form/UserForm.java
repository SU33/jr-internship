package tev.jr.form;

/**
 * Created by evg-us-jrcrud on 03.07.17.
 */
public class UserForm {
    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}