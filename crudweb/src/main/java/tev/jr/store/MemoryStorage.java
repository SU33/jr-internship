package tev.jr.store;

import tev.jr.models.User;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by evg-us-jrcrud on 03.07.17.
 */
public class MemoryStorage implements Storage<User> {

    private final AtomicInteger ids = new AtomicInteger();

    private final ConcurrentHashMap<Integer, User> users = new ConcurrentHashMap<Integer, User>();


    public Collection<User> values() {
        return this.users.values();
    }

    public int add(final User user) {
        this.users.put(user.getIdSpecial(), user);
        return user.getIdSpecial();
    }

    public void edit(final User user) {
        this.users.replace(user.getIdSpecial(), user);
    }

    public void delete(final int id) {
        this.users.remove(id);
    }

    public User get(final int id) {
        return this.users.get(id);
    }

    public User findByLogin(final String login) {
        for (final User user : users.values()) {
            if (user.getNameSpecial().equals(login)) {
                return user;
            }
        }
        throw new IllegalStateException(String.format("Login %s not found", login));
    }

    public int generateId() {
        return this.ids.incrementAndGet();
    }

    public void close() {
    }
}