package tev.jr.store;

import tev.jr.models.Role;

/**
 * Created by evg-us-jrcrud on 02.07.17.
 */
public interface RoleDAO<R> extends Storage<Role> {

}
