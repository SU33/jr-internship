package tev.jr.store;

import java.util.Collection;

/**
 * Created by evg-us-jrcrud on 30.06.17.
 */
public interface Storage<T> {

    public Collection<T> values();

    public int add(final T user);

    public void edit(final T user);

    public void delete(final int id);

    public T get(final int id);

    public T findByLogin(final String login) ;

    public int generateId();

    public void close();
}