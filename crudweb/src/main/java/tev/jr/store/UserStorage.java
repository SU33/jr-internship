package tev.jr.store;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;
import tev.jr.models.User;

import java.util.Collection;
import java.util.List;

/**
 * Created by evg-us-jrcrud on 30.06.17.
 */
@Repository
public class UserStorage implements Storage<User> {
    private SessionFactory factory;

    //pattern command (start)
    public interface Command<T> {
        T process(Session session);
    }

    private <T> T transaction(final Command<T> command) {
        final Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            return command.process(session);
        } finally {
            transaction.commit();
            close();
        }
    }
    //pattern command (end)


    public UserStorage() {
        factory = new Configuration().configure().buildSessionFactory();
    }

    public Collection<User> values() {
        return transaction(new Command<Collection<User>>() {
            public Collection<User> process(Session session) {
                return session.createQuery("from User").list();
            }
        });
    }

    public int add(final User user) {
        return transaction(new Command<Integer>() {
            public Integer process(Session session) {
                session.save(user);
                return user.getIdSpecial();
            }
        });
    }

    public void edit(final User user) {
        transaction(new Command<Void>() {
            public Void process(Session session) {
                session.update(user);
                return null;
            }
        });
    }

    public void delete(final int id) {
        transaction(new Command<Void>() {
            public Void process(Session session) {
                session.delete(session.get(User.class, id));
                return null;
            }
        });
    }

    public User get(final int id) {
        return transaction(new Command<User>() {
            public User process(Session session) {
                return (User) session.get(User.class, id);
            }
        });
    }

    public User findByLogin(final String login) {
        return transaction(new Command<User>() {
            public User process(Session session) {
                Query query = session.createQuery("from User as us where us.nameSpecial=:p_login");
                query.setString("p_login", login);
                List<User> userList = query.list();
                return (userList.isEmpty()) ? null : userList.iterator().next();
            }
        });
    }

    public List<User> searchByLogin(final String login) {
        return transaction(new Command<List<User>>() {
            public List<User> process(Session session) {
                Query query = session.createQuery("from User as us where us.nameSpecial like %:p_loign%");
                query.setString("p_loign", login);
                return query.list();
            }
        });
    }

    public int generateId() {
        return 0;
    }

    public void close() {
        factory.close();
    }
}
