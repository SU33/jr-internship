package tev.jr.store;

import tev.jr.models.User;

import java.util.Collection;

/**
 * Created by evg-us-jrcrud on 03.07.17.
 */
public class UserCache implements Storage<User> {
    private static final UserCache INSTANCE = new UserCache();

    private final Storage<User> storage = new MemoryStorage();

    public static UserCache getInstance() {
        return INSTANCE;
    }

    public Collection<User> values() {
        return storage.values();
    }

    public int add(final User user) {
        return this.storage.add(user);
    }

    public void edit(final User user) {
        this.storage.edit(user);
    }

    public void delete(final int id) {
        this.storage.delete(id);
    }

    public User get(final int id) {
        return this.storage.get(id);
    }

    public User findByLogin(final String login) {
        return this.storage.findByLogin(login);
    }


    public int generateId() {
        return this.storage.generateId();
    }

    public void close() {
        this.storage.close();
    }
}