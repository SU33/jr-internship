package tev.jr.store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by evg-us-jrcrud on 02.07.17.
 */
@Service
public class Storages {
    public final UserStorage userStorage;
    public final RoleStorage roleStorage;

    @Autowired
    public Storages(final UserStorage userStorage, final RoleStorage roleStorage) {
        this.userStorage = userStorage;
        this.roleStorage = roleStorage;
    }
}
