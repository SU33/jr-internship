package tev.jr.store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import tev.jr.models.Role;

import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
//можем уже не создавать поля сессий, транзакций... код стал при помощи spring template

/**
 * Created by evg-us-jrcrud on 02.07.17.
 */
@Repository
public class RoleStorage implements RoleDAO<Role> {
    private final HibernateTemplate template;

    @Autowired
    public RoleStorage(final HibernateTemplate template) {
        this.template = template;
    }


    public Collection<Role> values() {
        return (List<Role>) this.template.find("from Role");
    }

    @Transactional
    public int add(Role user) {
        template.save(user);
        return 1;
    }

    public void edit(Role user) {
    }

    public void delete(int id) {

    }

    public Role get(int id) {
        return null;
    }

    public Role findByLogin(String login) {
        return null;
    }

    public int generateId() {
        return 0;
    }

    public void close() {

    }
}
