package tev.jr.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by evg-us-jrcrud on 26.06.17.
 */
public class CrudServlet extends HttpServlet{
    private static ArrayList<String> specialList = new ArrayList<String>();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        specialList.add(req.getParameter("bars"));
        doGet(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Title</title>\n" +
                "</head>\n" +
                "<body>\n" +
                " <form action='"+req.getContextPath()+"/' method='post'>\n" +
                        "        Name: <input type='text' name = 'bars'>\n" +
                        "        <input type='submit' value='submitingMy'>\n" +
                        "    </form>" +
                getTableOfList()+
                "</body>\n" +
                "</html>");
        writer.flush();
    }

    private static String getTableOfList() {
        StringBuilder sb = new StringBuilder();
        sb.append("<table>");
        int count = 0;
        for (String element : specialList) {
            count++;
            sb.append("<tr><td>Element#"+count+"</td><td>"+element+"</td></tr>");
        }
        sb.append("</table>");
        return (count != 0) ? sb.toString() : "";
    }
}
