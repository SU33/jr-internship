package tev.jr.store;

import org.junit.Test;
import tev.jr.models.User;
import tev.jr.store.UserStorage;

import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * Created by evg-us-jrcrud on 01.07.17.
 */
public class UserStorageTest {
    @Test
    public void testCreate() {
        final UserStorage storage = new UserStorage();
        final int id = storage.add(new User(11, "hiberTest", 1, false,new Date()));
        final User user = storage.get(id);
        assertEquals(id, user.getIdSpecial());
        assertEquals(id, storage.findByLogin("hiberTest").getIdSpecial());
        storage.delete(id);
        assertNull(storage.get(id));
        storage.close();
    }
}
